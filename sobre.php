

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>fashion store</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="assets/css/icon-font.min.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Helper CSS -->
    <link rel="stylesheet" href="assets/css/helper.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>
</head>

<body>

    <div class="main-wrapper">

        <!-- Header Section Start -->
        <div class="header-section section">

            <!-- Header Top Start -->
            <div class="header-top header-top-one bg-theme-two">
                <div class="container-fluid">
                    <div class="row align-items-center justify-content-center">

                        <div class="col mt-10 mb-10 d-none d-md-flex">
                            <!-- Header Top Left Start -->
                            <div class="header-top-left">
                                <p>Welcome to fashion store</p>
                                <p>Hotline: <a href="tel:0123456789">0123 456 789</a></p>
                            </div><!-- Header Top Left End -->
                        </div>



                        <div class="col mt-10 mb-10">
                            <!-- Header Shop Links Start -->
                            <div class="header-top-right">

                                <p><a href="my-account.html">Minha conta</a></p>
                                <p><a href="login-register.html">Cadastre-se</a><a href="login-register.html">Login</a>
                                </p>

                            </div><!-- Header Shop Links End -->
                        </div>

                    </div>
                </div>
            </div><!-- Header Top End -->

            <?php include ('menu.php') ?>

        </div><!-- Header Section End -->

    </div><!-- Banner Section End -->

    <div class="page-banner-section section" style="background-image: url(assets/images/hero/hero-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="page-banner-content col">

                    <h1>CONTATOS</h1>
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="./">CONTATOS </a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
  <!-- Page Section Start -->
  <div class="page-section section section-padding">
        <div class="container">
            <div class="row row-30 mbn-40">

               <div class="contact-info-wrap col-md-6 col-12 mb-40">
                   <h3>Get in Touch</h3>
                   <p>fashion store é a melhor loja com suas variedades de roupas para você, que procura estilo e conforto</p>
                   <ul class="contact-info">
                       <li>
                           <i class="fa fa-map-marker"></i>
                           <p>256, 1st AVE, You address <br>will be here</p>
                       </li>
                       <li>
                           <i class="fa fa-phone"></i>
                           <p><a href="#">+01 235 567 89</a><a href="#">+01 235 286 65</a></p>
                       </li>
                       <li>
                           <i class="fa fa-globe"></i>
                           <p><a href="#">info@example.com</a><a href="#">www.example.com</a></p>
                       </li>
                   </ul>
               </div>

               <div class="contact-form-wrap col-md-6 col-12 mb-40">
                   <h3>Leave a Message</h3>
                   <form  id="contact-form" action="assets/php/mail.php">
                       <div class="contact-form">
                           <div class="row">
                               <div class="col-lg-6 col-12 mb-30"><input type="text" name="name" placeholder="Your Name"></div>
                               <div class="col-lg-6 col-12 mb-30"><input type="email" name="email" placeholder="Email Address"></div>
                               <div class="col-12 mb-30"><textarea name="message" placeholder="Message"></textarea></div>
                               <div class="col-12"><input type="submit" value="send"></div>
                           </div>
                       </div>
                   </form>
                   <p class="form-messege"></p>
               </div>

            </div>
        </div>
    </div><!-- Page Section End -->
    
    <?php include ('footer.php') ?>


    <!-- Footer Bottom Section Start -->
    <div class="footer-bottom-section section bg-theme-two pt-15 pb-15">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <p class="footer-copyright">Copyright &copy; All rights reserved</p>
                </div>
            </div>
        </div>
    </div><!-- Footer Bottom Section End -->

    </div>

    <!-- JS
============================================ -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.4.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Plugins JS -->
    <script src="assets/js/plugins.js"></script>
    <!-- Ajax Mail -->
    <script src="assets/js/ajax-mail.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

</body>

</html>