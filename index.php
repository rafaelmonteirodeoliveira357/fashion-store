<?php

session_start();

include("conecta.php");

if(isset($_POST['btnAdicionar'])) {

    if(isset($_SESSION['datahora'])) {
        mysqli_query($conecta, "INSERT INTO tmpvendas (datahora, fkidProduto, quantidade) VALUES (
            '" . $_SESSION['datahora'] . "',
            '" . $_POST['txtCodigoProduto'] . "',
            1
        )");
    } else {

        $_SESSION['datahora']=date('l jS \of F Y h:i:s A');
        mysqli_query($conecta, "INSERT INTO tmpvendas (datahora, fkidProduto, quantidade) VALUES (
            '" . $_SESSION['datahora'] . "',
            '" . $_POST['txtCodigoProduto'] . "',
            1
        )");
    }

}

if(isset($_GET['ref'])) {

    if(isset($_SESSION['datahora'])) {
        mysqli_query($conecta, "INSERT INTO tmpvendas (datahora, fkidProduto, quantidade) VALUES (
            '" . $_SESSION['datahora'] . "',
            '" . base64_decode($_GET['ref']) . "',
            1
        )");
    } else {

        $_SESSION['datahora']=date('l jS \of F Y h:i:s A');
        mysqli_query($conecta, "INSERT INTO tmpvendas (datahora, fkidProduto, quantidade) VALUES (
            '" . $_SESSION['datahora'] . "',
            '" . base64_decode($_GET['ref']) . "',
            1
        )");
    }

}

if(isset($_POST['btnmensagem'])) {
    mysqli_query($conecta, "INSERT INTO mensagem (nome, email, mensagem) VALUES (

        '" . $_POST['txtnome'] . "',
        '" . $_POST['txtemail'] . "',
        '" . $_POST['txtmensagem'] . "'
    )");

    header("location: .");
}
?>


<!doctype html>
<html class="no-js" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Fashion Store</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="assets/css/icon-font.min.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Helper CSS -->
    <link rel="stylesheet" href="assets/css/helper.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>
</head>

<body>

    <div class="main-wrapper">

        <!-- Header Section Start -->
        <div class="header-section section">

            <?php include ('menu.php') ?>

        </div><!-- Header Section End -->

        <!-- Hero Section Start -->
        <div class="hero-section section">

            <!-- Hero Slider Start -->
            <div class="hero-slider hero-slider-one fix">

                <!-- Hero Item Start -->
                <div class="hero-item" style="background-image: url(img-banner/banner-desconto.png)">

                    <!-- Hero Content -->
                    <div class="hero-content" style="background: transparent">

                        <h1 style="color: transparent">Ganhe 35% de desconto <br>Último produto para bebês</h1>
                        <a href="#" style="display: none">COMPRE AGORA</a>

                    </div>

                </div><!-- Hero Item End -->

                <!-- Hero Item Start -->
                <div class="hero-item" style="background-image: url(img-banner/banner-contato.png)" img="">

                    <!-- Hero Content -->
                    <div class="hero-content" style="background: transparent">

                        <h1 style="color: transparent">Ganhe 35% de desconto<br>Último produto para bebês</h1>
                        <a href="#" style="display: none">COMPRE AGORA</a>

                    </div>

                </div><!-- Hero Item End -->

            </div><!-- Hero Slider End -->

        </div><!-- Hero Section End -->

    </div><!-- Banner Section End -->

    <!-- Product Section Start -->
    <div class="product-section section section-padding">
        <div class="container">

            <div class="row">
                <div class="section-title text-center col mb-30">
                    <h1>Produtos populares</h1>
                    <p>Todos os produtos populares encontram aqui</p>
                </div>
            </div>

            <div class="row mbn-40">

                <?php

                    $query=mysqli_query($conecta, "SELECT * FROM produtos ORDER BY pkId LIMIT 8");
                    while($result=mysqli_fetch_assoc($query)) {

                ?>

                <div class="col-xl-3 col-lg-4 col-md-6 col-12 mb-40">

                    <div class="product-item">
                        <div class="product-inner">
                            <form method="post">

                                <div class="image">
                                    <img src="painel/produtos/<?php echo $result['foto'] ?>" alt="">

                                    <div class="image-overlay">
                                        <div class="action-buttons">
                                            <input type="hidden" name="txtCodigoProduto"
                                                value="<?php echo $result['pkid'] ?>" />
                                            <button type="submit" name="btnAdicionar">adicionar carrinho</button>
                                        </div>
                                    </div>

                                </div>
                            </form>

                            <div class="content">

                                <div class="content-left">

                                    <h4 class="title"><a
                                            href="single-product.html"><?php echo ($result['nome']) ?></a>
                                    </h4>

                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>

                                    <h5 class="size">Tamanho: <?php echo ($result['tamanho']) ?>
                                    </h5>
                                    <h5 class="color">Color: <span style="background-color: #ffb2b0"></span><span
                                            style="background-color: #0271bc"></span><span
                                            style="background-color: #efc87c"></span><span
                                            style="background-color: #00c183"></span></h5>

                                </div>

                                <div class="content-right">
                                    <span class="price">$<?php echo intval($result['preco']) ?></span>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>

                <?php } ?>

            </div>

        </div>
    </div><!-- Product Section End -->


    <!-- Product Section Start -->
    <div class="product-section section section-padding pt-0">
        <div class="container">
            <div class="row mbn-40">

                <div class="col-lg-4 col-md-6 col-12 mb-40">

                    <div class="row">
                        <div class="section-title text-left col mb-30">
                            <h1>Melhor negócio</h1>
                            <p>Ofertas exclusivas para você</p>
                        </div>
                    </div>

                    <div class="best-deal-slider w-100">

                        <?php

                        $query=mysqli_query($conecta, "SELECT * FROM produtos ORDER BY pkId LIMIT 9, 5");
                        while($result=mysqli_fetch_assoc($query)) {

                    ?>

                        <div class="slide-item">
                            <div class="best-deal-product">

                                <div class="image"><img src="painel/produtos/<?php echo $result['foto'] ?>"
                                        alt=""></div>

                                <div class="content-top">

                                    <div class="content-top-left">
                                        <h4 class="title"><a href="#"
                                                style="color: white; text-shadow: 1px 1px black"><?php echo ($result['nome']) ?></a>
                                        </h4>
                                        <div class="ratting">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                    </div>

                                    <div class="content-top-right">
                                        <span class="price"
                                            style="text-shadow: 1px 1px black; font-size: 35px">$<?php echo intval($result['preco']) ?></span>
                                    </div>

                                </div>

                                <div class="content-bottom">
                                    <a href=".?ref=<?php echo base64_encode($result['pkid']) ?>"
                                        style="background: #FFF; padding: 10px;">COMPRAR</a>
                                </div>

                            </div>
                        </div>

                        <?php } ?>

                    </div>

                </div>

                <div class="col-lg-8 col-md-6 col-12 pl-3 pl-lg-4 pl-xl-5 mb-40">

                    <div class="row">
                        <div class="section-title text-left col mb-30">
                            <h1>Produtos à venda</h1>
                            <p>Todos os produtos em destaque encontram-se aqui</p>
                        </div>
                    </div>

                    <div class="small-product-slider row row-7 mbn-40">

                        <?php

                        $query=mysqli_query($conecta, "SELECT * FROM produtos ORDER BY pkId LIMIT 13, 8");
                        while($result=mysqli_fetch_assoc($query)) {

                    ?>
                        <div class="col mb-40">

                            <div class="on-sale-product">
                                <a href="produto_detalhes.php?ref=<?php echo base64_encode($result['pkid'])?>"
                                    class="image" style="height: 230px; border: 1px solid #CCC"><img
                                        src="painel/produtos/<?php echo $result['foto'] ?>"
                                        alt="<?php echo ($result['nome']) ?>"></a>
                                <div class="content text-center">
                                    <h4 class="title"><a
                                            href="produto_detalhes.php?ref=<?php echo base64_encode($result['pkid'])?>"><?php echo substr(($result['nome']), 0, 25) . "..." ?></a>
                                    </h4>
                                    <span class="price">$<?php echo intval($result['preco']) ?> <span
                                            class="old">$<?php echo intval($result['preco'] + 50) ?></span></span>

                                </div>
                            </div>

                        </div>

                        <?php } ?>

                    </div>

                </div>

            </div>
        </div>
    </div><!-- Product Section End -->

    <!-- Feature Section Start -->
    <div class="feature-section bg-theme-two section section-padding fix"
        style="background-image: url(assets/images/pattern/pattern-dot.png);">
        <div class="container">
            <div class="feature-wrap row justify-content-between mbn-30">

                <div class="col-md-4 col-12 mb-30">
                    <div class="feature-item text-center">

                        <div class="icon"><img src="assets/images/feature/feature-1.png" alt=""></div>
                        <div class="content">
                            <h3>Envio Grátis</h3>
                            <p>A partir de $ 100</p>
                        </div>

                    </div>
                </div>

                <div class="col-md-4 col-12 mb-30">
                    <div class="feature-item text-center">

                        <div class="icon"><img src="assets/images/feature/feature-2.png" alt=""></div>
                        <div class="content">
                            <h3>Garantia de devolução de dinheiro</h3>
                            <p>De volta em 25 dias</p>
                        </div>

                    </div>
                </div>

                <div class="col-md-4 col-12 mb-30">
                    <div class="feature-item text-center">

                        <div class="icon"><img src="assets/images/feature/feature-3.png" alt=""></div>
                        <div class="content">
                            <h3>Pagamento seguro</h3>
                            <p>Segurança de pagamento</p>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div><!-- Feature Section End -->


    <!-- Blog Section Start -->
    <div class="blog-section section section-padding">
        <div class="container">
            <div class="row mbn-40">

                <div class="col-xl-12 col-lg-5 col-12 mb-40">

                    <div class="row">
                        <div class="section-title text-left col mb-30">
                            <h1>REVISÃO DE CLIENTES</h1>
                            <p>Os clientes dizem sobre nós</p>
                        </div>
                    </div>

                    <?php
                        $query=mysqli_query($conecta, "SELECT * FROM mensagem");
                        while($result=mysqli_fetch_assoc($query)) {
                    ?>
                    <div class="row mb-40">
                    
                        <div class="col-12">
                            <div class="testimonial-item">
                                <p><?php echo ($result['mensagem']) ?></p>
                                <div class="testimonial-author">

                                    <div class="content" style="margin-top: -25px">
                                        <h4><?php echo $result['nome'] ?></h4>
                                        <p><?php echo $result['email'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php } ?>


                </div>

                <div class="col-xl-6 col-lg-7 col-12 mb-40">
                    <form method="post">
                        <fieldset>
                            <legend>Deixe sua Opinião sobre a loja.</legend>
                            <div class="mb-3">
                                <label for="disabledTextInput" class="form-label">NOME</label>
                                <input type="text" id="disabledTextInput" class="form-control" required name="txtnome" 
                                    placeholder="DIGITE NOME">
                            </div>
                            <div class="mb-3">
                                <label for="disabledTextInput" class="form-label">EMAIL</label>
                                <input type="email" class="form-control" required name="txtemail"
                                    placeholder="DIGITE SEU EMAIL">
                            </div>
                            <div class="mb-3">
                                < <label for="disabledTextInput" class="form-label">MENSAGEM</label>
                                    <input type="text" class="form-control" required name="txtmensagem"
                                        placeholder="DIGITE SUA MENSAGEM">
                            </div>

                            <button type="submit" class="btn btn-primary" name="btnmensagem">Submit</button>
                        </fieldset>
                    </form>
                </div>

            </div>

        </div>
    </div>

    </div><!-- Blog Section End -->



    <?php include ('footer.php') ?>

    </div>

    <!-- JS
============================================ -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.4.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Plugins JS -->
    <script src="assets/js/plugins.js"></script>
    <!-- Ajax Mail -->
    <script src="assets/js/ajax-mail.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

</body>

</html>