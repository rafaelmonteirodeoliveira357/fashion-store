<?php


include('conecta.php');
session_start();

if(isset($_POST['btnLogin'])) {
    $email=$_POST['txtEmail'];
    $senha=sha1($_POST['txtSenha']);
    $query=mysqli_query($conecta, "SELECT * FROM clientes WHERE email='$email' AND senha='$senha'");
    
    if(mysqli_num_rows($query) >= 1) {
        $result=mysqli_fetch_assoc($query);
        session_start();
        $_SESSION['nome']=$result['nome'];
        $_SESSION['id']=$result['pkid'];
        header("Location: index.php");
    }
}


if(isset($_POST['btnCadastrar'])) {
    $cadastra=mysqli_query($conecta, "INSERT INTO clientes (nome, email, telefone, rua, bairro, cidade, uf, cep, cpf, senha) values (
    '" . $_POST['txtNome'] . "',
    '" . $_POST['txtEmail'] . "',
    '" . $_POST['txtTelefone'] . "',
    '" . $_POST['txtRua'] . "',
    '" . $_POST['txtBairro'] . "',
    '" . $_POST['txtCidade'] . "',
    '" . $_POST['txtUF'] . "',
    '" . $_POST['txtCep'] . "',
    '" . $_POST['txtCPF'] . "',
    '" . sha1($_POST['txtSenha']) . "'
    )");
    $query=mysqli_query($conecta, "SELECT * FROM clientes WHERE email='".$_POST['txtEmail']."' AND senha='".sha1($_POST['txtSenha'])."'");
    
    if(mysqli_num_rows($query) >= 1) {
        $result=mysqli_fetch_assoc($query);
        session_start();
        $_SESSION['nome']=$result['nome'];
        $_SESSION['id']=$result['pkid'];
        header("Location: index.php");
    }
}

?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>fashion store</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="assets/css/icon-font.min.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Helper CSS -->
    <link rel="stylesheet" href="assets/css/helper.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>
</head>

<body>

    <div class="main-wrapper">

        <!-- Header Section Start -->
        <div class="header-section section">

            <?php include ('menu.php') ?>

        </div><!-- Header Section End -->

    </div><!-- Banner Section End -->

    <div class="page-banner-section section" style="background-image: url(img-banner/banner-cadastrese.png)">
        <div class="container">
            <div class="row">
                <div class="page-banner-content col">

                    <ul class="page-breadcrumb">
                        <li><a href="index.php">CASA</a></li>
                        <li><a href="./">CADASTRE-SE</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
  <!-- Page Section Start -->
    <!-- Page Section Start -->
    <div class="page-section section section-padding">
        <div class="container">
            <div class="row mbn-40">

                <div class="col-lg-4 col-12 mb-40">
                    <div class="login-register-form-wrap">
                        <h3>Login</h3>
                        <form class="mb-30" method="post">
                            <div class="row">
                                <div class="col-12 mb-15"><input type="text" name="txtEmail" required placeholder="Nome de usuário ou email"></div>
                                <div class="col-12 mb-15"><input type="password" name="txtSenha" required placeholder="senha"></div>
                                <div class="col-12"><input type="submit" name="btnLogin"></div>
                            </div>
                        </form>
                        <h4>Você também pode fazer login com...</h4>
                        <div class="social-login">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-12 mb-40 text-center">
                    <span class="login-register-separator"></span>
                </div>
                

                <div class="col-lg-6 col-12 mb-40 ml-auto">
                    <div class="login-register-form-wrap">
                        <h3>CADASTRE-SE</h3>
                        <form method="post">
                        <script>
                        function limpa_formulário_cep() {
                            //Limpa valores do formulário de cep.
                            document.getElementById('rua').value = ("");
                            document.getElementById('bairro').value = ("");
                            document.getElementById('cidade').value = ("");
                            document.getElementById('uf').value = ("");
                        }

                        function meu_callback(conteudo) {
                            if (!("erro" in conteudo)) {
                                //Atualiza os campos com os valores.
                                document.getElementById('rua').value = (conteudo.logradouro);
                                document.getElementById('bairro').value = (conteudo.bairro);
                                document.getElementById('cidade').value = (conteudo.localidade);
                                document.getElementById('uf').value = (conteudo.uf);
                            } //end if.
                            else {
                                //CEP não Encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        }

                        function pesquisacep(valor) {

                            //Nova variável "cep" somente com dígitos.
                            var cep = valor.replace(/\D/g, '');

                            //Verifica se campo cep possui valor informado.
                            if (cep != "") {

                                //Expressão regular para validar o CEP.
                                var validacep = /^[0-9]{8}$/;

                                //Valida o formato do CEP.
                                if (validacep.test(cep)) {

                                    //Preenche os campos com "..." enquanto consulta webservice.
                                    document.getElementById('rua').value = "...";
                                    document.getElementById('bairro').value = "...";
                                    document.getElementById('cidade').value = "...";
                                    document.getElementById('uf').value = "...";

                                    //Cria um elemento javascript.
                                    var script = document.createElement('script');

                                    //Sincroniza com o callback.
                                    script.src = 'https://viacep.com.br/ws/' + cep + '/json/?callback=meu_callback';

                                    //Insere script no documento e carrega o conteúdo.
                                    document.body.appendChild(script);

                                } //end if.
                                else {
                                    //cep é inválido.
                                    limpa_formulário_cep();
                                    alert("Formato de CEP inválido.");
                                }
                            } //end if.
                            else {
                                //cep sem valor, limpa formulário.
                                limpa_formulário_cep();
                            }
                        };
                        </script>

                            <div class="row">
                                <div class="col-md-12 col-12 mb-15"><input name="txtNome" type="text" placeholder="Digite seu nome"></div>
                                <div class="col-md-6 col-12 mb-15"><input name="txtTelefone" type="text" placeholder="Telefone"></div>
                                <div class="col-md-6 col-12 mb-15"><input name="txtCPF" type="text" placeholder="CPF"></div>
                                <div class="col-md-12 col-12 mb-15"><input id="cep" name="txtCep" type="text" placeholder="12356-789" onblur="pesquisacep(this.value)"></div>
                                <div class="col-md-12 col-12 mb-15"><input id="rua" name="txtRua" type="text" placeholder="R. Exemplo"></div>
                                <div class="col-md-12 col-12 mb-15"><input id="bairro" name="txtBairro" type="text" placeholder="Bairro Exemplo"></div>
                                <div class="col-md-12 col-12 mb-15"><input id="cidade" name="txtCidade" type="text" placeholder="Taubaté"></div>
                                <div class="col-md-12 col-12 mb-15"><input id="uf" name="txtUF" type="text" placeholder="SP"></div>
                                <div class="col-md-6 col-12 mb-15"><input name="txtEmail" type="email" placeholder="Email"></div>
                                <div class="col-md-6 col-12 mb-15"><input name="txtSenha" type="text" placeholder="Digite sua senha"></div>
                                <div class="col-md-6 col-12"><input type="submit" name="btnCadastrar" value="CADASTRAR"></div>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div><!-- Page Section End -->

    
    <?php include ('footer.php') ?>

    </div>

    <!-- JS
============================================ -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.4.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Plugins JS -->
    <script src="assets/js/plugins.js"></script>
    <!-- Ajax Mail -->
    <script src="assets/js/ajax-mail.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

</body>

</html>