<?php

session_start();

include("conecta.php");

if(isset($_POST['btnAdicionar'])) {

    if(isset($_SESSION['datahora'])) {
        mysqli_query($conecta, "INSERT INTO tmpvendas (datahora, fkidProduto, quantidade) VALUES (
            '" . $_SESSION['datahora'] . "',
            '" . $_POST['txtCodigoProduto'] . "',
            '" . $_POST['txtQuantidade'] . "'
        )");
    } else {

        $_SESSION['datahora']=date('l jS \of F Y h:i:s A');
        mysqli_query($conecta, "INSERT INTO tmpvendas (datahora, fkidProduto, quantidade) VALUES (
            '" . $_SESSION['datahora'] . "',
            '" . $_POST['txtCodigoProduto'] . "',
            '" . $_POST['txtQuantidade'] . "'
        )");
    }

}
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>fashion store</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="assets/css/icon-font.min.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Helper CSS -->
    <link rel="stylesheet" href="assets/css/helper.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>
</head>

<body>

    <div class="main-wrapper">

        <!-- Header Section Start -->
        <div class="header-section section">

            <?php include ('menu.php') ?>

        </div><!-- Header Section End -->

    </div><!-- Banner Section End -->

    <div class="page-banner-section section" style="background-image: url(img-banner/banner-informacoes.png)">
        <div class="container">
            <div class="row">
                <div class="page-banner-content col">

                    <h1>PRODUTO</h1>
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="./">Produto Detalhes </a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <!-- Page Section Start -->


    <!-- Page Section Start -->
    <div class="page-section section section-padding">
        <div class="container">
            <div class="row row-30 mbn-50">

                <?php
                    $query=mysqli_query($conecta, "SELECT * FROM produtos WHERE pkid=".base64_decode($_GET['ref']));
                    $result=mysqli_fetch_assoc($query);
                ?>

                <div class="col-12">
                    <div class="row row-20 mb-10">

                        <div class="col-lg-6 col-12 mb-40">

                            <div class="pro-large-img mb-10 fix easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                                <a href="painel/produtos/<?php echo $result['foto'] ?>">
                                    <img src="painel/produtos/<?php echo $result['foto'] ?>" alt="" />
                                </a>
                            </div>
                            <!-- Single Product Thumbnail Slider -->
                            <ul id="pro-thumb-img" class="pro-thumb-img">
                                <li><a href="painel/produtos/<?php echo $result['foto'] ?>"
                                        data-standard="painel/produtos/<?php echo $result['foto'] ?>"><img
                                            src="painel/produtos/<?php echo $result['foto'] ?>" alt="" /></a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-lg-6 col-12 mb-40">
                            <div class="single-product-content">

                                <div class="head">
                                    <div class="head-left">

                                        <h3 class="title"><?php echo utf8_encode($result['nome']) ?></h3>

                                        <div class="ratting">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>

                                    </div>

                                    <div class="head-right">
                                        <span class="price">$<?php echo $result['preco'] ?></span>
                                    </div>
                                </div>

                                <span class="availability">Em estoque:
                                    <span><?php echo $result['estoque'] ?></span></span>

                                <form method="post">
                                    <div class="quantity-colors">

                                        <div class="quantity">
                                            <h5>Quantidade:</h5>
                                            <div class="pro-qty">
                                                <input type="text" value="1" name="txtQuantidade" >
                                                <input type="hidden" value="<?php echo $result['pkid'] ?>" name="txtCodigoProduto" >
                                            </div>
                                        </div>

                                        <div class="colors">
                                            <h5>Color:</h5>
                                            <div class="color-options">
                                                <button style="background-color: #ff502e"></button>
                                                <button style="background-color: #fff600"></button>
                                                <button style="background-color: #1b2436"></button>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="actions">

                                        <button name="btnAdicionar"><i class="ti-shopping-cart"></i><span>ADICIONAR AO
                                                CARRINHO</span></button>


                                    </div>
                                </form>


                            </div>
                        </div>

                    </div>


                </div>

            </div>
        </div>
    </div><!-- Page Section End -->

    <?php include ('footer.php') ?>


    </div>

    <!-- JS
============================================ -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.4.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Plugins JS -->
    <script src="assets/js/plugins.js"></script>
    <!-- Ajax Mail -->
    <script src="assets/js/ajax-mail.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

</body>

</html>