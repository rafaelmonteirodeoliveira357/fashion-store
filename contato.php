

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>fashion store</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="assets/css/icon-font.min.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Helper CSS -->
    <link rel="stylesheet" href="assets/css/helper.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>
</head>

<body>


            </div><!-- Header Top End -->

            <?php include ('menu.php') ?>

        </div><!-- Header Section End -->

    </div><!-- Banner Section End -->

    <div class="page-banner-section section" style="background-image: url(img-banner/banner-contato.png)">
        <div class="container">
            <div class="row">
                <div class="page-banner-content col">

                   
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="./">CONTATOS </a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
  <!-- Page Section Start -->
  <div class="page-section section section-padding">
        <div class="container">
            <div class="row row-30 mbn-40">

               <div class="contact-info-wrap col-md-6 col-12 mb-40">
                   <h3>Entrar em contato</h3>
                   <p>fashion store é a melhor loja com suas variedades de roupas para você, que procura estilo e conforto</p>
                   <ul class="contact-info">
                       <li>
                           <i class="fa fa-map-marker"></i>
                           <p>R. prof. nelson campello, 202 - jardin Eulalio, Taubaté - SP, 12010-700o<br>estará aqui</p>
                       </li>
                       <li>
                           <i class="fa fa-phone"></i>
                           <p><a href="#">(12)21256099</a></p>
                       </li>
                       <li>
                           <i class="fa fa-globe"></i>
                           <p><a href="#">fashionstore@gmail.com</a></p>
                       </li>
                   </ul>
               </div>

               <div class="contact-form-wrap col-md-6 col-12 mb-40">
                   <h3>Deixe um recado</h3>
                   <form  id="contact-form" action="assets/php/mail.php">
                       <div class="contact-form">
                           <div class="row">
                               <div class="col-lg-6 col-12 mb-30"><input type="text" name="name" placeholder="Seu nome"></div>
                               <div class="col-lg-6 col-12 mb-30"><input type="email" name="email" placeholder="Seu email"></div>
                               <div class="col-12 mb-30"><textarea name="mensagem" placeholder="mensagem"></textarea></div>
                               <div class="col-12"><input type="submit" value="Mandar"></div>
                           </div>
                       </div>
                   </form>
                   <p class="form-messege"></p>
               </div>

            </div>
        </div>
    </div><!-- Page Section End -->
    
    <?php include ('footer.php') ?>


    <!-- Footer Bottom Section Start -->
    <div class="footer-bottom-section section bg-theme-two pt-15 pb-15">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <p class="footer-copyright">direito autoral &copy; Todos os direitos reservados</p>
                </div>
            </div>
        </div>
    </div><!-- Footer Bottom Section End -->

    </div>

    <!-- JS
============================================ -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.4.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Plugins JS -->
    <script src="assets/js/plugins.js"></script>
    <!-- Ajax Mail -->
    <script src="assets/js/ajax-mail.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

</body>

</html>