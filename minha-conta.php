<?php
	session_start();
	include('conecta.php');

	?>



<!doctype html>
<html class="no-js" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>fashion store</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>

    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="assets/css/icon-font.min.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Helper CSS -->
    <link rel="stylesheet" href="assets/css/helper.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>
</head>

<body>

    <div class="main-wrapper">

        <!-- Header Section Start -->
        <div class="header-section section">

            <?php include ('menu.php') ?>

        </div><!-- Header Section End -->

    </div><!-- Banner Section End -->

    <div class="page-banner-section section" style="background-image: url(img-banner/banner-minhaconta.png)">
        <div class="container">
            <div class="row">
                <div class="page-banner-content col">

                    <ul class="page-breadcrumb">
                        <li><a href="index.php">CASA</a></li>
                        <li><a href="./">CADASTRE-SE</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <!-- Page Section Start -->

    <!-- Page Section Start -->
    <div class="page-section section section-padding">
        <div class="container">
            <div class="row mbn-30">

                <!-- My Account Tab Menu Start -->
                <div class="col-lg-3 col-12 mb-30">
                    <div class="myaccount-tab-menu nav" role="tablist">
                        <a href="#dashboad" data-toggle="tab"><i class="fa fa-dashboard"></i>
                            Painel</a>

                        <a href="#orders" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i> Orders</a>

                        <a href="#payment-method" data-toggle="tab"><i class="fa fa-credit-card"></i> Payment
                            Method</a>
                        <!-- 			
	                    <a href="#download" data-toggle="tab"><i class="fa fa-cloud-download"></i> Download</a>
                        
						<a href="#address-edit" data-toggle="tab"><i class="fa fa-map-marker"></i> address</a> -->

                        <a href="#account-info" class="active" data-toggle="tab"><i class="fa fa-user"></i> Detalhes da
                            conta</a>

                        <a href="logout.php"><i class="fa fa-sign-out"></i> sair</a>
                    </div>
                </div>
                <!-- My Account Tab Menu End -->

                <!-- My Account Tab Content Start -->
                <div class="col-lg-9 col-12 mb-30">
                    <div class="tab-content" id="myaccountContent">
                        <!-- Single Tab Content Start -->
                        <div class="tab-pane fade show " id="dashboad" role="tabpanel">
                            <div class="myaccount-content">
                                <h3>Painel</h3>

                                <div class="welcome">
                                    <p><strong>Olá, <?php echo $_SESSION['nome'] ?> </strong> (Se não for você <a
                                            href="logout.php" class="logout"> , <strong>sair</strong></a>)</p>
                                </div>

                                <p class="mb-0">
                                    Olá! (Bem vindo a sua conta!)
                                    você pode facilmente verificar e visualizar seus pedidos recentes, gerenciar seus
                                    endereços de envio e cobrança e editar sua senha e detalhes da conta.</p>
                            </div>
                        </div>
                        <!-- Single Tab Content End -->

                        <!-- Single Tab Content Start -->
                        <div class="tab-pane fade" id="orders" role="tabpanel">
                            <div class="myaccount-content">
                                <h3>Orders</h3>

                                <div class="myaccount-table table-responsive text-center">
                                    <table class="table table-bordered">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Data</th>
                                                <th>Nome</th>
                                                <th>Quantidade</th>
                                                <th>Total</th>
                                                <th>Produto</th>
                                            </tr>
                                        </thead>

                                        <?php
										
											$busca=mysqli_query($conecta, "SELECT a.data, b.*, c.nome, c.preco, c.foto  FROM vendas a, vendasitens b, produtos c WHERE c.pkid=b.idProduto AND a.idCliente='".$_SESSION['id']."' AND b.idVenda=a.pkid");
                                            
											while($result=mysqli_fetch_assoc($busca)) {
										?>

                                        <tbody>
                                            <tr>
                                                <td><?php echo $result['data'] ?></td>
                                                <td><?php echo $result['nome'] ?></td>
                                                <td><?php echo $result['quantidade'] ?></td>
                                                <td><?php echo number_format( $result['preco'] *  $result['quantidade'], 2)  ?>
                                                </td>
                                                <td><a data-bs-toggle="modal" data-bs-target="#modalProduto">
                                                        <img src="Painel/produtos/<?php echo $result['foto']?>"
                                                            height="50px" alt="">
                                                    </a></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>

                                    </table>

                                </div>

                            </div>

                        </div>

                        <!-- Single Tab Content End -->

                        <!-- Single Tab Content Start -->
                        <!-- <div class="tab-pane fade" id="download" role="tabpanel">
							<div class="myaccount-content">
								<h3>Downloads</h3>

								<div class="myaccount-table table-responsive text-center">
									<table class="table table-bordered">
										<thead class="thead-light">
										<tr>
											<th>Product</th>
											<th>Date</th>
											<th>Expire</th>
											<th>Download</th>
										</tr>
										</thead>

										<tbody>
										<tr>
											<td>Moisturizing Oil</td>
											<td>Aug 22, 2018</td>
											<td>Yes</td>
											<td><a href="#" class="btn btn-dark btn-round">Download File</a></td>
										</tr>
										<tr>
											<td>Katopeno Altuni</td>
											<td>Sep 12, 2018</td>
											<td>Never</td>
											<td><a href="#" class="btn btn-dark btn-round">Download File</a></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div> -->
                        <!-- Single Tab Content End -->

                        <!-- Single Tab Content Start -->
                        <div class="tab-pane fade" id="payment-method" role="tabpanel">
                            <div class="myaccount-content">
                                <h3>Payment Method</h3>

                                <p class="saved-message">You Can't Saved Your Payment Method yet.</p>
                            </div>
                        </div>
                        <!-- Single Tab Content End -->

                        <!-- Single Tab Content Start -->
                        <div class="tab-pane fade" id="address-edit" role="tabpanel">
                            <div class="myaccount-content">
                                <h3>Billing Address</h3>

                                <address>
                                    <p><strong>Alex Tuntuni</strong></p>
                                    <p>1355 Market St, Suite 900 <br>
                                        San Francisco, CA 94103</p>
                                    <p>Mobile: (123) 456-7890</p>
                                </address>

                                <a href="#" class="btn btn-dark btn-round d-inline-block"><i class="fa fa-edit"></i>Edit
                                    Address</a>
                            </div>
                        </div>
                        <!-- Single Tab Content End -->

                        <!-- Single Tab Content Start -->
                        <div class="tab-pane fade active" id="account-info" role="tabpanel">
                            <div class="myaccount-content">
                                <h3>DETALHES DA CONTA</h3>

                                <?php 

                                    $query=mysqli_query($conecta, "SELECT * FROM clientes WHERE pkid='".$_SESSION['id']."'");
    
									$result=mysqli_fetch_assoc($query);
 
                                 ?>

                                <div class="account-details-form">
                                    <form action="#">
                                        <div class="row">
                                            <div class="col-lg-6 col-12 mb-30">
                                                <input id="first-name" placeholder="nome" type="text"
                                                    value="<?php echo !empty($result['nome']) ? $result['nome'] : '' ?>">
                                            </div>

                                            <div class="col-lg-6 col-12 mb-30">
                                                <input id="last-name" placeholder="Email" type="text"
                                                    value="<?php echo !empty($result['email']) ? $result['email'] : '' ?>">
                                            </div>

                                            <div class="col-12 mb-30">
                                                <input id="display-name" placeholder="Endereço" type="text"
                                                    value="<?php echo $result['rua'] . ", " . $result['bairro'] . " - " . $result['cidade'] . "/" . $result['uf']?>">
                                            </div>

                                            <div class="col-lg-6 col-12 mb-30">
                                                <input id="first-name" placeholder="CPF" type="text"
                                                    value="<?php echo !empty($result['cpf']) ? $result['cpf'] : '' ?>">
                                            </div>

                                            <div class="col-lg-6 col-12 mb-30">
                                                <input id="last-name" placeholder="Telefone" type="text"
                                                    value="<?php echo !empty($result['telefone']) ? $result['telefone'] : '' ?>">
                                            </div>


                                            <div class="col-12 mb-30">
                                                <h4>TROCAR SENHA</h4>
                                            </div>

                                            <div class="col-lg-6 col-12 mb-30">
                                                <input id="new-pwd" placeholder="Nova senha" type="password">
                                            </div>

                                            <div class="col-lg-6 col-12 mb-30">
                                                <input id="confirm-pwd" placeholder="Confirmar senha" type="password">
                                            </div>

                                            <div class="col-12">
                                                <button class="btn btn-dark btn-round btn-lg">Salvar alterçães</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Single Tab Content End -->
                    </div>
                </div>
                <!-- My Account Tab Content End -->

            </div>
        </div>
    </div><!-- Page Section End -->

    <?php include ('footer.php') ?>





    <div class="modal" id="modalProduto" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    </div>

    <!-- JS
============================================ -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.4.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Plugins JS -->
    <script src="assets/js/plugins.js"></script>
    <!-- Ajax Mail -->
    <script src="assets/js/ajax-mail.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

</body>

</html>