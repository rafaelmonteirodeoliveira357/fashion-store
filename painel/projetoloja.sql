-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19-Fev-2022 às 01:57
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetoloja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `pkid` int(11) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `telefone` varchar(120) NOT NULL,
  `endereco` text NOT NULL,
  `cpf` varchar(120) NOT NULL,
  `senha` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`pkid`, `nome`, `email`, `telefone`, `endereco`, `cpf`, `senha`) VALUES
(1, 'Glauco Luiz Parquet', 'glauco@glauco.com', '12 991991991', 'R. teste, 105 - bairro nao', '777.777.777-77', '40bd001563085fc35165329ea1ff5c5ecbdbbeef'),
(2, 'Nickolas Domingos', 'nickinho@gmail.com', '12 996090909', 'R. Não sei, 43 - Bairro sei Não', '123.456.789-00', '123');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionarios`
--

CREATE TABLE `funcionarios` (
  `pkid` int(11) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `telefone` varchar(120) NOT NULL,
  `login` varchar(120) NOT NULL,
  `senha` varchar(120) NOT NULL,
  `acesso` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `funcionarios`
--

INSERT INTO `funcionarios` (`pkid`, `nome`, `email`, `telefone`, `login`, `senha`, `acesso`) VALUES
(1, 'Administrador', 'admin@admin.com', '(12) 99999-9999', 'admin', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'TOTAL'),
(2, 'joao', 'joao@joao.com', '12121212121', 'joao', ' 40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'TOTAL');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `pkid` int(11) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `preco` varchar(50) NOT NULL,
  `tamanho` varchar(10) NOT NULL,
  `estoque` int(13) NOT NULL,
  `foto` text NOT NULL,
  `categoria` varchar(25) NOT NULL,
  `numero` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`pkid`, `nome`, `preco`, `tamanho`, `estoque`, `foto`, `categoria`, `numero`) VALUES
(1, 'Camiseta EUA', '49,00', 'G', 15, 'fotos/camisetaEUA.jpg', 'masculino', ''),
(2, 'Camiseta São Paulo', '210,00', 'G', 120, 'fotos/camiseta-saopaulo.jpg', 'feminino', ''),
(5, 'Sapato Preto', '120,00', '', 25, 'fotos/sapato preto.jpg', 'calcados', '43'),
(6, 'Bolsa Preta', '50,00', '', 25, 'fotos/bolsa-preta.jfif', 'acessorios', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas`
--

CREATE TABLE `vendas` (
  `pkid` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `status` varchar(15) NOT NULL,
  `motivo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `vendas`
--

INSERT INTO `vendas` (`pkid`, `idCliente`, `data`, `hora`, `total`, `status`, `motivo`) VALUES
(1, 1, '2022-02-17', '20:00:00', '120.00', 'Recusado', 'Falta de Estoque'),
(2, 2, '2022-02-17', '20:00:00', '150.00', 'Pedido Recebido', ''),
(3, 1, '2022-02-17', '20:00:00', '170.00', 'Pedido Recebido', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendasitens`
--

CREATE TABLE `vendasitens` (
  `pkid` int(11) NOT NULL,
  `idVenda` int(11) NOT NULL,
  `idProduto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `vendasitens`
--

INSERT INTO `vendasitens` (`pkid`, `idVenda`, `idProduto`, `quantidade`) VALUES
(1, 3, 2, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`pkid`);

--
-- Indexes for table `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD PRIMARY KEY (`pkid`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`pkid`);

--
-- Indexes for table `vendas`
--
ALTER TABLE `vendas`
  ADD PRIMARY KEY (`pkid`);

--
-- Indexes for table `vendasitens`
--
ALTER TABLE `vendasitens`
  ADD PRIMARY KEY (`pkid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `pkid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `funcionarios`
--
ALTER TABLE `funcionarios`
  MODIFY `pkid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `pkid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `vendas`
--
ALTER TABLE `vendas`
  MODIFY `pkid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vendasitens`
--
ALTER TABLE `vendasitens`
  MODIFY `pkid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
