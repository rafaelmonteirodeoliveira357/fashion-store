<?php

$tela=1;

include('../conexao.php');

session_start();

if (isset($_POST['btnExcluir'])){
    mysqli_query($conecta, "DELETE FROM funcionarios WHERE pkid=".$_POST['idUserDelete']);
    header("Location: .");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Projeto :: Cadastro de Usuarios</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
        integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

    <link rel="stylesheet" href="../estilo.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
</head>

<body>

    <nav class="navbar navbar-light bg-light p-3">

        <?php include('../header.php') ?>

    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav id="sidebar" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">

                <?php include('../menu.php') ?>

            </nav>
            <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4 py-4">
                <nav aria-label="breadcrumb" style="line-height: 60px;">
                    <ol class="breadcrumb" style="padding-left: 25px">
                        <li class="breadcrumb-item"><a href="../home.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Cadastro de Usuários</li>
                    </ol>
                </nav>
                <h1 class="h2">Usuários</h1>
                <p>Cadastro de usuários</p>
                <a href="cadastro-usuario.php" class="btn btn-sm btn-outline-primary"
                    style="float:right; margin-top:-40px">Novo Cadastro</a>
                <div class="row">
                    <div class="col-12 col-xl-12 mb-4 mb-lg-0">
                        <div class="card">
                            <h5 class="card-header">Relação de Usuários</h5>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Nome</th>
                                                <th scope="col">Email</th>
                                                <th scope="col">Telefone</th>
                                                <th scope="col">Login</th>
                                                <th scope="col">Acesso</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
											$busca=mysqli_query($conecta, "SELECT * FROM funcionarios");
											while($result=mysqli_fetch_assoc($busca)) {
										?>

                                            <tr>
                                                <th scope="row"> <?php echo $result['nome'] ?> </th>
                                                <td><?php echo $result['email'] ?></td>
                                                <td><?php echo $result['telefone'] ?></td>
                                                <td><?php echo $result['login'] ?></td>
                                                <td><?php echo $result['acesso'] ?></td>
                                                <td>
                                                    <a href="edita-usuario.php?ref=<?php echo $result['pkid']?>"
                                                        class="btn btn-sm btn-outline-primary">Editar</a>
                                                    <a href="#" class="btn btn-sm btn-outline-danger"
                                                        data-bs-toggle="modal" data-bs-target="#deleteModal"
                                                        onclick="passaDados(<?php echo $result['pkid']?>, '<?php echo $result['nome']?>')">Excluir</a>
                                                </td>
                                            </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php include('../footer.html') ?>

            </main>

            <script>
            function passaDados(id, nome) {
                document.getElementById("idUser").value = id;
                document.getElementById("nomeUser").value = nome;
                document.getElementById("idUserDelete").value = id;
            }
            </script>

            <div class="modal" tabindex="-1" id="deleteModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post">
                            <div class="modal-header">
                                <h5 class="modal-title">Deseja Mesmo Exluir?</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <p>Deseja excluir permanentemente: <br>
                                    <input style="border:none; width:100%" disabled type="text" id="nomeUser" /><br>
                                    id= <input style="border:none; width:30px" disabled type="text" id="idUser" />
                                    <input style="border:none; width:30px" name="idUserDelete" type="hidden" id="idUserDelete"/>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Fechar</button>
                                <button name="btnExcluir" type="submit" class="btn btn-outline-primary">Excluir</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
        integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>

</body>

</html>