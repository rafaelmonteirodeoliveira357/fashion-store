<?php

$tela=1;

include('../conexao.php');

session_start();

//TESTA SE O BOTAO SALVAR FOI CLICADO
if(isset($_POST['btnEnviar'])) {

    $nome=$_POST['txtNome'];
    $email=$_POST['txtEmail'];
    $senha=sha1($_POST['txtSenha']);
    $telefone=$_POST['txtTelefone'];
    $login=$_POST['txtLogin'];
    $acesso=$_POST['txtAcesso'];
    //INSERE USUARIO NO BANCO DE DADOS
    $query=mysqli_query($conecta, "UPDATE funcionarios SET nome='$nome', email='$email', senha='$senha', telefone='$telefone', login='$login', acesso='$acesso' WHERE pkid=".$_GET['ref']);

    if($query){
        $color=base64_encode('success');
        $status=base64_encode('Que beleza!');
        $msg=base64_encode('Cadastro atualizado com sucesso!');
    } else {
        $color=base64_encode('danger');
        $status=base64_encode('Que nhaca!');
        $msg=base64_encode('Cadastro deu ruim!');
    }

    header('Location: .?msg='.$msg.'&status='.$status.'&color='.$color);
    exit;

}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Projeto :: Cadastro de Usuarios</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
        integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

    <link rel="stylesheet" href="estilo.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
</head>

<body>

    <nav class="navbar navbar-light bg-light p-3">

        <?php include('../header.php') ?>

    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav id="sidebar" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">

                <?php include('../menu.php') ?>

            </nav>
            <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4 py-4">
                <nav aria-label="breadcrumb" style="line-height: 60px;">
                    <ol class="breadcrumb" style="padding-left: 25px">
                        <li class="breadcrumb-item"><a href="./home.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Cadastro de Usuários</li>
                    </ol>
                </nav>
                <h1 class="h2">Usuários</h1>
                <p>Cadastro de usuários</p>
                <div class="row">
                    <div class="col-12 col-xl-8 mb-4 mb-lg-0">


                        <?php 
                            if(isset($_GET['msg'])) {
                        ?>

                        <div class="alert alert-<?php echo base64_decode($_GET['color']) ?> alert-dismissible fade show"
                            role="alert">
                            <strong><?php echo base64_decode($_GET['status']) ?></strong>
                            <?php echo base64_decode($_GET['msg']) ?>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>

                        <?php } ?>
                         <?php 
                        $query=mysqli_query($conecta, "SELECT * FROM funcionarios WHERE pkid=".$_GET['ref']);
                        $result=mysqli_fetch_assoc($query)?>       
                        <form method="post">

                            <div class="mb-3 mt-2">
                                <label class="form-label">Nome</label>
                                <input type="text" required name="txtNome" class="form-control"
                                    placeholder="Nome do usuario"
                                     value="<?php echo $result ["nome"]?>">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Email</label>
                                <input type="email" required name="txtEmail" class="form-control"
                                    placeholder="Ex: test@test.com"
                                    value="<?php echo $result ["email"]?>">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Telefone</label>
                                <input type="text" required name="txtTelefone" class="form-control"
                                    placeholder="Ex: 12 123456789"
                                    value="<?php echo $result ["telefone"]?>">
                                    
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Login</label>
                                <input type="text" required name="txtLogin" class="form-control"
                                    placeholder="Ex:abc"
                                    value="<?php echo $result ["login"]?>">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Senha</label>
                                <input type="text" required name="txtSenha" class="form-control"
                                    placeholder="Ex: 123Mudar">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Acesso</label>
                                <select class="form-select form-select-md" name="txtAcesso"
                                    aria-label=".form-select-sm example">
                                    <option selected value="<?php echo $result ["acesso"]?>"><?php echo $result ["acesso"]?></option>
                                    <option value="TOTAL">Total</option>
                                    <option value="RESTRITO">Restrito</option>
                                </select>
                            </div>

                            <div class="mt-4">
                                <button type="submit" name="btnEnviar" class="btn btn-outline-primary">Salvar</button>
                                <a href="." name="btnCancelar" class="btn btn-outline-danger">Cancelar</a>
                            </div>

                        </form>

                    </div>
                </div>

                <?php include('../footer.html') ?>

            </main>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
        integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>

</body>

</html>