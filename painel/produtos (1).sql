-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 25-Fev-2022 às 00:16
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetoloja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `pkid` int(11) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `preco` varchar(50) NOT NULL,
  `tamanho` varchar(10) NOT NULL,
  `estoque` int(13) NOT NULL,
  `foto` text NOT NULL,
  `categoria` varchar(25) NOT NULL,
  `numero` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`pkid`, `nome`, `preco`, `tamanho`, `estoque`, `foto`, `categoria`, `numero`) VALUES
(1, 'Camiseta EUA', '45,00', 'G', 100, 'fotos/camisetaEUA.jpg', 'Masculino', ''),
(3, 'Vestido azul ', '75', 'P', 97, 'fotos/PicsArt_02-02-07.39.19.jpg', 'Infantil', ''),
(4, 'Boddy com saia de tule  preto', '87', 'M', 100, 'fotos/PicsArt_02-02-07.39.32.jpg', 'Infantil', ''),
(5, 'Boddy verde ', '65', 'P', 100, 'fotos/PicsArt_02-02-07.40.47.jpg', 'Infantil', ''),
(6, 'Macacão rosa', '99', 'M', 100, 'fotos/PicsArt_02-02-07.41.03.jpg', 'infantil', ''),
(7, 'Conjunto de tigre azul claro', '68', 'M', 100, 'fotos/PicsArt_02-02-07.41.32.jpg', 'infantil', ''),
(8, 'Macacão azul escuro ', '69,99', 'G', 100, 'fotos/PicsArt_02-02-07.41.49.jpg', 'infantil', ''),
(9, 'Biquíni sereia infantil colorido', '78', 'P', 100, 'fotos/PicsArt_02-02-07.42.02.jpg', 'infantil', ''),
(10, 'Conjunto saia marrom e blusa de manga longo preta', '68', 'M', 100, 'fotos/PicsArt_02-02-07.42.15.jpg', 'infantil', ''),
(11, 'Conjunto cropped e shorts ', '99,99', 'M', 100, 'fotos/PicsArt_02-02-07.42.30.jpg', 'infantil', ''),
(12, 'Conjunto cropped e calça florida', '145', 'M', 100, 'fotos/PicsArt_02-02-07.43.48.jpg', 'infantil', ''),
(13, 'Conjunto cropped preto e calça preta ', '77,99', 'G', 100, 'fotos/PicsArt_02-02-07.44.02.jpg', 'infantil', ''),
(15, 'Conjunto cropped branco e saia jeans ', '68', 'G', 100, 'fotos/PicsArt_02-02-07.44.17 (2).jpg', 'infantil', ''),
(16, 'Conjunto camisa regata e shorts de basquete', '80', 'M', 100, 'fotos/PicsArt_02-02-07.45.17.jpg', 'infantil', ''),
(18, 'Conjunto Camisa e calça moletom ', '75', 'M', 100, 'fotos/PicsArt_02-02-07.45.31.jpg', 'infantil', ''),
(19, 'Conjunto camisa preta e shorts bege', '88', 'G', 100, 'fotos/PicsArt_02-02-07.45.47 (1).jpg', 'infantil', ''),
(20, 'Camisa estampa games ', '60', 'M', 100, 'fotos/PicsArt_02-02-07.46.26.jpg', 'infantil', ''),
(21, 'Shorts moletom ', '70', 'G', 100, 'fotos/PicsArt_02-02-07.46.54.jpg', 'infantil', ''),
(22, 'Conjunto camisa preta e shorts listrado ', '100', 'G', 100, 'fotos/PicsArt_02-02-07.47.06.jpg', 'infantil', ''),
(23, 'Tênis Nike dunk low cinza ', '800', '', 97, 'fotos/CV1659-001.png', 'calcados', '41'),
(24, 'Tênis Nike dunk low ', '1200', '', 100, 'fotos/f7a3b9_b94607832d764d569a987aa4d2663730_mv2.jpg', 'calcados', '39'),
(25, 'Tênis preto e branco ', '500', '', 100, 'fotos/PicsArt_02-02-07.36.45.jpg', 'calcados', '40'),
(26, 'Tênis cano alto preto', '400', '', 100, 'fotos/PicsArt_02-02-07.37.10 (1).jpg', 'calcados', '36'),
(27, 'Salto branco', '900', '', 100, 'fotos/PicsArt_02-02-07.38.42.jpg', 'calcados', '35'),
(28, 'Salto preto', '800', '', 100, 'fotos/PicsArt_02-02-07.39.05.jpg', 'calcados', '36'),
(30, 'Sandalia infantil feminina', '140', '', 100, 'fotos/PicsArt_02-02-07.39.43.jpg', 'calcados', '18'),
(31, 'sandalia infantil feminina azul', '160', '', 100, 'fotos/PicsArt_02-02-07.41.17 (1).jpg', 'calcados', '18'),
(32, 'Salto rosa ', '900', '', 100, 'fotos/PicsArt_02-02-07.54.40.jpg', 'calcados', '36'),
(33, 'Sapato infantil ', '190', '', 100, 'fotos/PicsArt_02-02-07.55.15 (1).jpg', 'calcados', '20'),
(34, 'Sapato preto', '170', '', 100, 'fotos/PicsArt_02-02-07.55.35.jpg', 'calcados', '18'),
(36, 'Botina infantil', '100', '', 100, 'fotos/PicsArt_02-02-07.55.46.jpg', 'calcados', '20'),
(37, 'Sandalia infantil amarela ', '180', '', 100, 'fotos/PicsArt_02-02-07.56.25.jpg', 'Calcados', '20'),
(38, 'Tênis fashion ', '500', '', 100, 'fotos/PicsArt_02-02-07.56.36 (1).jpg', 'calcados', '39'),
(39, 'Sandalia infantil feminina', '240', '', 100, 'fotos/PicsArt_02-02-07.56.48 (2).jpg', 'calcados', '18'),
(40, 'Sandalia infantil feminina rosa', '100', '', 100, 'fotos/PicsArt_02-02-07.57.27 (1).jpg', 'calcados', '18'),
(41, 'Tênis nike dunk low roxo', '800', '', 100, 'fotos/tenis_nike_sb_dunk_low_court_purple_2372_2_84c6ac83aeeb15a295349a13912dc718 (2).jpg', 'calcados', '42'),
(42, 'Tênis nike air max cinza ', '700', '', 100, 'fotos/tenis-nike-air-max-95-essential-masculino-CI3705-600-1 (1).jpg', 'calcados', '40'),
(43, 'Tênis dunk preto', '1000', '', 100, 'fotos/tenis-nike-sportswear-air-force-1-07-lx-feminino-DD1525-001-1-11635533896 (1).jpg', 'calcados', '41'),
(44, 'Shorts moletom nike preto', '170', 'G', 100, 'fotos/636a7a8b82ac4d48639b5a34711034af (1).jpg', 'masculino', ''),
(45, 'Camisa de time paris saint germain(PSG)', '300', 'M', 100, 'fotos/84084ccf351c17e3f7ecad7ed8d64776 (1).jpg', 'Masculino', ''),
(46, 'Calça cargo branca', '150', 'M', 100, 'fotos/123480442_970683700083923_5086092201954949693_n (1).jpg', 'masculino', ''),
(47, 'Calça cargo preta', '80', 'M', 100, 'fotos/150941681_440069047143850_2988002623823217644_n (1).jpg', 'Feminino', ''),
(48, 'Calça cargo verde', '100', 'G', 100, 'fotos/194323913_154173413358045_1566389133176336623_n (2).jpg', 'masculino', ''),
(49, 'Camiseta anime', '130', 'G', 100, 'fotos/1617788664ed2fceb04481226c87891197e08d9570_thumbnail_600x (1).webp', 'masculino', ''),
(50, 'Camiseta anime ', '80', 'M', 100, 'fotos/1622022229db6a504269d3595a0b926f91e7bd8215_thumbnail_600x (1).webp', 'masculino', ''),
(51, 'Camiseta preta ', '130', 'M', 100, 'fotos/1630320432e943ffbbf417f60e7b4ad0988b103ef8_thumbnail_600x (1).webp', 'Masculino', ''),
(52, 'Camiseta anime ', '75', 'M', 100, 'fotos/16167463146f7796a8bdcb835bf15f0dde163c736e_thumbnail_600x (2).webp', 'masculino', ''),
(53, 'Camiseta preta ', '90', 'G', 100, 'fotos/16426572615d4e85825cb8aedf91b6a629d8304e03_thumbnail_600x (1).webp', 'masculino', ''),
(54, 'Camiseta preta', '90', 'M', 100, 'fotos/162337464579aec8d731e002d543f03f5f344e4459_thumbnail_600x (1).webp', 'masculino', ''),
(55, 'Camiseta preta', '99,90', 'G', 100, 'fotos/163549523841024ae238cbbecfc96faa24c58abc3e_thumbnail_600x (2).webp', 'masculino', ''),
(56, 'Shorts moletom nike preto', '89', 'M', 100, 'fotos/HZM-1024-026_zoom1 (2).jpg', 'masculino', ''),
(57, 'Shorts moletom nike cinza', '89', 'M', 100, 'fotos/HZM-1024-028_zoom1 (1).jpg', 'masculino', ''),
(59, 'Saia colegial preta', '150', 'M', 97, 'fotos/PicsArt_02-01-09.15.48 (2).jpg', 'feminino', ''),
(60, 'Calça cargo preta', '150', 'G', 100, 'fotos/PicsArt_02-01-09.16.14 (1).jpg', 'feminino', ''),
(61, 'Calça de alfaiataria', '120', 'M', 100, 'fotos/PicsArt_02-01-09.16.29 (1).jpg', 'feminino', ''),
(62, 'Calça wide leg ', '100', 'M', 100, 'fotos/PicsArt_02-01-09.17.12 (1).jpg', 'feminino', ''),
(63, 'calça jeans preta', '180', 'G', 100, 'fotos/PicsArt_02-01-09.17.25 (2).jpg', 'feminino', ''),
(64, 'Cropped preto de borboleta ', '80', 'P', 100, 'fotos/PicsArt_02-01-09.17.49 (1).jpg', 'feminino', ''),
(65, 'Cropped preto ', '80', 'M', 100, 'fotos/PicsArt_02-01-09.18.08 (2).jpg', 'feminino', ''),
(66, 'Cropped de manga curta ', '87', 'G', 100, 'fotos/PicsArt_02-01-09.18.22 (1).jpg', 'feminino', ''),
(67, 'Cropped de manga longa', '100', 'M', 100, 'fotos/PicsArt_02-01-09.19.49 (1).jpg', 'feminino', ''),
(68, 'Vestido preto de dragão ', '240', 'M', 100, 'fotos/PicsArt_02-01-09.20.03 (2).jpg', 'feminino', ''),
(69, 'Vestido colegial preto ', '350', 'G', 100, 'fotos/PicsArt_02-01-09.20.15 (2).jpg', 'feminino', ''),
(70, 'Conjunto cropped e calça cinza ', '230', 'G', 100, 'fotos/PicsArt_02-01-09.20.28 (1).jpg', 'Feminino', ''),
(71, 'Conjunto cropped e shorts curto', '220', 'M', 100, 'fotos/PicsArt_02-01-09.20.41 (2).jpg', 'feminino', ''),
(72, 'xuxinha de cabelo', '15', '', 100, 'fotos/IMG-20220202-WA0007.jpg', 'acessorios', ''),
(73, 'Pulseira de nome', '35', '', 100, 'fotos/IMG-20220202-WA0008.jpg', 'acessorios', ''),
(74, 'Prendedor de cabelo', '25', '', 100, 'fotos/IMG-20220202-WA0009.jpg', 'acessorios', ''),
(75, 'óculos escuro', '135', '', 100, 'fotos/PicsArt_02-01-09.20.56.jpg', 'acessorios', ''),
(76, 'bucket preto', '100', '', 100, 'fotos/PicsArt_02-01-09.21.08.jpg', 'acessorios', ''),
(77, 'cinta de couro', '200', '', 100, 'fotos/PicsArt_02-01-09.22.08.jpg', 'acessorios', ''),
(78, 'Corrente de prata ', '350', '', 100, 'fotos/PicsArt_02-01-09.22.28 (1).jpg', 'acessorios', ''),
(79, 'Grampo de cabelo', '200', '', 100, 'fotos/PicsArt_02-01-09.22.40.jpg', 'acessorios', ''),
(80, 'Bandana preta ', '115', '', 100, 'fotos/PicsArt_02-02-08.11.48.jpg', 'acessorios', ''),
(81, 'Chapéu De Palha', '150', '', 100, 'fotos/PicsArt_02-02-08.11.59.jpg', 'acessorios', ''),
(82, 'touca e luva rosa', '200', '', 100, 'fotos/PicsArt_02-02-08.12.11.jpg', 'acessorios', ''),
(83, 'Tiara de princesa ', '80', '', 100, 'fotos/PicsArt_02-02-08.12.51.jpg', 'acessorios', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`pkid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `pkid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
