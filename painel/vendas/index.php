<?php

$tela=1;

include('../conexao.php');

session_start();

if (isset($_POST['btnRecusar'])){
    mysqli_query($conecta, "UPDATE vendas SET motivo='".$_POST['txtMotivo']."', status='Recusado' WHERE pkid=".$_POST['idUserRecusa']);
    header("Location: .");
}

if (isset($_POST['btnDetalhes'])){
    header("Location: detalhes.php?ref=".base64_encode($_POST['txtCodigoVenda']));
}

if (isset($_POST['btnAprovar'])){
    mysqli_query($conecta, "UPDATE vendas SET status='Pedido Aprovado' WHERE pkid=".$_POST['txtCodigoVenda']);
    $query=mysqli_query($conecta,"SELECT * FROM vendasItens WHERE idVenda=".$_POST['txtCodigoVenda']);
    while ($result=mysqli_fetch_assoc($query)) { 
        mysqli_query($conecta, "UPDATE produtos SET estoque = (estoque - ".$result['quantidade'].") WHERE pkid=".$result['idProduto']);
    } 
    header("Location: .");
}

if (isset($_GET['id'])){
    mysqli_query($conecta, "UPDATE vendas SET status='Em transito' WHERE pkid=".$_GET['id']);
    header("Location: .");
}   

if (isset($_GET['idEntrega'])){
    mysqli_query($conecta, "UPDATE vendas SET status='Entregue' WHERE pkid=".$_GET['idEntrega']);
    header("Location: .");
}   
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Projeto :: Relação de Vendas</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
        integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

    <link rel="stylesheet" href="../estilo.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
</head>

<body>

    <nav class="navbar navbar-light bg-light p-3">

        <?php include('../header.php') ?>

    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav id="sidebar" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">

                <?php include('../menu.php') ?>

            </nav>
            <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4 py-4">
                <nav aria-label="breadcrumb" style="line-height: 60px;">
                    <ol class="breadcrumb" style="padding-left: 25px">
                        <li class="breadcrumb-item"><a href="../home.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Relação de Vendas</li>
                    </ol>
                </nav>
                <h1 class="h2">Vendas</h1>
                <p>Relação de Vendas</p>


                <div class="row">
                    <div class="col-12 col-xl-12 mb-4 mb-lg-0">
                        <div class="card">
                            <h5 class="card-header">Relação de Vendas</h5>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Código</th>
                                                <th scope="col">Cliente</th>
                                                <th scope="col">Data/Hora</th>
                                                <th scope="col">Total</th>
                                                <th scope="col">Status</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
											$busca=mysqli_query($conecta, "
                                            SELECT v.pkid, v.status, c.nome, DATE_FORMAT(v.data,'%d/%m/%Y') AS data,  DATE_FORMAT(v.hora,'%H:%i') AS hora, v.total
                                            FROM vendas AS v
                                            JOIN clientes AS c ON v.idCliente=c.pkid
                                            ");
											while($result=mysqli_fetch_assoc($busca)) {
										?>

                                            <tr>
                                                <th scope="row"> <?php echo $result['pkid'] ?> </th>
                                                <td><?php echo $result['nome'] ?></td>
                                                <td><?php echo $result['data'] . ' - ' . $result['hora'] ?></td>
                                                <td><?php echo $result['total'] ?></td>
                                                <td><?php echo $result['status'] ?></td>
                                                <td>
                                                    <?php 
                                                    if($result['status'] !="Recusado" && $result['status'] !="Pedido Aprovado" && $result['status'] !="Em transito"  && $result['status'] !="Entregue"){?>
                                                    <a href="#" class="btn btn-sm btn-outline-primary"
                                                        data-bs-toggle="modal" data-bs-target="#aprovaModal"
                                                        onclick="aprovaDados(<?php echo $result['pkid']?>, '<?php echo $result['nome']?>')">Aprovar</a>

                                                    <a href="#" class="btn btn-sm btn-outline-danger"
                                                        data-bs-toggle="modal" data-bs-target="#recusaModal"
                                                        onclick="passaDados(<?php echo $result['pkid']?>, '<?php echo $result['nome']?>')">Recusar</a>
                                                    <?php } ?>
                                                    <?php 
                                                  if($result['status'] !="Recusado" && $result['status'] !="Em transito"  && $result['status'] !="Entregue" && $result['status'] !="Pedido Recebido"){?>
                                                    <a href="index.php?id=<?php echo $result['pkid']?>"
                                                        class="btn btn-sm btn-outline-primary">Em transito</a>
                                                    <?php } ?>
                                                    <?php
                                                    if($result['status'] !="Recusado" && $result['status'] !="Pedido Aprovado" && $result['status'] !="Entregue"  && $result['status'] !="Pedido Recebido"){?>
                                                    <a href="index.php?idEntrega=<?php echo $result['pkid']?>"
                                                        class="btn btn-sm btn-outline-primary">Entregue</a>
                                                    <?php } ?>
                                                </td>
                                            </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script>
                    function passaDados(id, nome) {
                        document.getElementById("idUser").value = id;
                        document.getElementById("nomeUser").value = nome;
                        document.getElementById("idUserRecusa").value = id;
                    }
                    </script>

                    <div class="modal" tabindex="-1" id="recusaModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form method="post">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Deseja Mesmo Recusar?</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Deseja recusar a compra de: <br>
                                            <input style="border:none; width:100%" disabled type="text"
                                                id="nomeUser" /><br>
                                            id venda= <input style="border:none; width:30px" disabled type="text"
                                                id="idUser" />
                                            <input style="border:none; width:30px" name="idUserRecusa" type="hidden"
                                                id="idUserRecusa" />
                                        <div class="mb-3">
                                            <label class="form-label">Motivo da Recusa</label>
                                            <input type="text" required name="txtMotivo" class="form-control"
                                                placeholder="Ex: Falta de Estoque">
                                        </div>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary"
                                            data-bs-dismiss="modal">Fechar</button>
                                        <button name="btnRecusar" type="submit"
                                            class="btn btn-outline-primary">Recusar</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>


                    <script>
                    function aprovaDados(id, nome) {
                        document.getElementById("idUserAprova").value = id;
                        document.getElementById("nomeUserAprova").value = nome;
                        document.getElementById("idUserAprova2").value = id;
                    }
                    </script>

                    <div class="modal" tabindex="-1" id="aprovaModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form method="post">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Aprovar Compra?</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Aprovar compra de: <br>
                                            <input style="border:none; width:100%" disabled type="text"
                                                id="nomeUserAprova" /><br>
                                            id= <input style="border:none; width:30px" disabled type="text"
                                                id="idUserAprova" />
                                            <input style="border:none; width:30px" name="txtCodigoVenda" type="hidden"
                                                id="idUserAprova2" />
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button name="btnAprovar" type="submit"
                                            class="btn btn-outline-primary">Aprovar</button>
                                        <button name="btnDetalhes" type="submit"
                                            class="btn btn-outline-warning">Detalhes</button>
                                        <button type="button" class="btn btn-outline-secondary"
                                            data-bs-dismiss="modal">Fechar</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <?php include('../footer.html') ?>

            </main>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
        integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>

</body>

</html>