<?php

include("conecta.php");

$tipoProduto = base64_decode($_GET['ref']);

if($tipoProduto === 'masculino') {
    $urlImagem='img-banner/banner-masculino.png';
} elseif ($tipoProduto === 'feminino') {
    $urlImagem='img-banner/banner-feminino.png';
} elseif ($tipoProduto === 'infantil') {
    $urlImagem='img-banner/banner-infantil.png';
} elseif ($tipoProduto === 'calcados') {
    $urlImagem='img-banner/banner-calcados.png';
} elseif ($tipoProduto === 'acessorios') {
    $urlImagem='img-banner/banner-acessorios.png';
}

session_start();
if(isset($_POST['btnAdicionar'])) {

    if(isset($_SESSION['datahora'])) {
        mysqli_query($conecta, "INSERT INTO tmpvendas (datahora, fkidProduto, quantidade) VALUES (
            '" . $_SESSION['datahora'] . "',
            '" . $_POST['txtCodigoProduto'] . "',
            1
        )");
    } else {

        $_SESSION['datahora']=date('l jS \of F Y h:i:s A');
        mysqli_query($conecta, "INSERT INTO tmpvendas (datahora, fkidProduto, quantidade) VALUES (
            '" . $_SESSION['datahora'] . "',
            '" . $_POST['txtCodigoProduto'] . "',
            1
        )");
    }

}

?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>fashion store</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="assets/css/icon-font.min.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Helper CSS -->
    <link rel="stylesheet" href="assets/css/helper.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>
</head>

<body>

    <div class="main-wrapper">

        <!-- Header Section Start -->
        <div class="header-section section">

            <?php include ('menu.php') ?>

        </div><!-- Header Section End -->

    </div><!-- Banner Section End -->

    <div class="page-banner-section section" style="background-image: url(<?php echo $urlImagem ?>)">
        <div class="container">
            <div class="row">
                <div class="page-banner-content col">

                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="./">Produtos : <?php echo $tipoProduto ?></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <!-- Page Section Start -->
    <div class="page-section section section-padding">
        <div class="container">

            <div class="row">

            
                <?php
                    $query=mysqli_query($conecta, "SELECT * FROM produtos WHERE categoria='" . $tipoProduto . "' ORDER BY pkId");
                    while($result=mysqli_fetch_assoc($query)) {

                ?>

                <div class="col-xl-3 col-lg-4 col-md-6 col-12 mb-40">

                    <div class="product-item">
                        <div class="product-inner">

                            <form method="post">

                                <div class="image" style="height: 300px">
                                    <img src="painel/produtos/<?php echo $result['foto'] ?>" alt="">

                                    <div class="image-overlay">
                                        <div class="action-buttons">
                                            <input type="hidden" name="txtCodigoProduto"
                                                value="<?php echo $result['pkid'] ?>" />
                                            <button type="submit" name="btnAdicionar">adicionar carrinho</button>
                                        </div>
                                    </div>

                                </div>
                            </form>

                            <div class="content">

                                <div class="content-left">

                                    <h4 class="title"><a
                                            href="produto_detalhes.php?ref=<?php echo base64_encode($result['pkid'])?>"><?php echo ($result['nome']) ?></a>
                                    </h4>

                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>

                                    <h5 class="size">TAMANHO: <span><?php echo $result['tamanho'] ?></span></h5>

                                </div>

                                <div class="content-right">
                                    <span class="price">$<?php echo $result['preco'] ?></span>
                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <?php } ?>

            </div>

        </div>
    </div><!-- Page Section End -->

    <?php include ('footer.php')?>

    </div>

    <!-- JS
============================================ -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.4.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Plugins JS -->
    <script src="assets/js/plugins.js"></script>
    <!-- Ajax Mail -->
    <script src="assets/js/ajax-mail.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

</body>

</html>