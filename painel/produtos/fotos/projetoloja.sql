-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17-Fev-2022 às 01:56
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetoloja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `pkid` int(11) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `telefone` varchar(120) NOT NULL,
  `endereco` text NOT NULL,
  `cpf` varchar(120) NOT NULL,
  `login` varchar(120) NOT NULL,
  `senha` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`pkid`, `nome`, `email`, `telefone`, `endereco`, `cpf`, `login`, `senha`) VALUES
(1, 'Glauco', 'glauco@glauco.com', '12 991991991', 'R. teste, 105 - bairro nao', '777.777.777-77', '', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionarios`
--

CREATE TABLE `funcionarios` (
  `pkid` int(11) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `telefone` varchar(120) NOT NULL,
  `login` varchar(120) NOT NULL,
  `senha` varchar(120) NOT NULL,
  `acesso` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `funcionarios`
--

INSERT INTO `funcionarios` (`pkid`, `nome`, `email`, `telefone`, `login`, `senha`, `acesso`) VALUES
(1, 'Administrador', 'admin@admin.com', '(12) 99999-9999', 'admin', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'TOTAL'),
(2, 'joao', 'joao@joao.com', '12121212121', 'joao', ' 40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'TOTAL');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `pkid` int(11) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `preco` varchar(50) NOT NULL,
  `tamanho` varchar(10) NOT NULL,
  `estoque` int(13) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`pkid`, `nome`, `preco`, `tamanho`, `estoque`, `foto`) VALUES
(1, 'Camiseta EUA', '45,00', 'G', 100, 'fotos/camisetaEUA.jpg'),
(3, 'Vestido azul ', '75', 'P', 100, 'fotos/PicsArt_02-02-07.39.19.jpg'),
(4, 'Boddy com saia de tule  preto', '87', 'M', 100, 'fotos/PicsArt_02-02-07.39.32.jpg'),
(5, 'Boddy verde ', '65', 'P', 100, 'fotos/PicsArt_02-02-07.40.47.jpg'),
(6, 'Macacão rosa', '99', 'M', 100, 'fotos/PicsArt_02-02-07.41.03.jpg'),
(7, 'Conjunto de tigre azul claro', '68', 'M', 100, 'fotos/PicsArt_02-02-07.41.32.jpg'),
(8, 'Macacão azul escuro ', '69,99', 'G', 100, 'fotos/PicsArt_02-02-07.41.49.jpg'),
(9, 'Biquíni sereia infantil colorido', '78', 'P', 100, 'fotos/PicsArt_02-02-07.42.02.jpg'),
(10, 'Conjunto saia marrom e blusa de manga longo preta', '68', 'M', 100, 'fotos/PicsArt_02-02-07.42.15.jpg'),
(11, 'Conjunto cropped e shorts ', '99,99', 'M', 100, 'fotos/PicsArt_02-02-07.42.30.jpg'),
(12, 'Conjunto cropped e calça florida', '145', 'M', 100, 'fotos/PicsArt_02-02-07.43.48.jpg'),
(13, 'Conjunto cropped preto e calça preta ', '77,99', 'G', 100, 'fotos/PicsArt_02-02-07.44.02.jpg'),
(15, 'Conjunto cropped branco e saia jeans ', '68', 'G', 100, 'fotos/PicsArt_02-02-07.44.17 (2).jpg'),
(16, 'Conjunto camisa regata e shorts de basquete', '80', 'M', 100, 'fotos/PicsArt_02-02-07.45.17.jpg'),
(18, 'Conjunto Camisa e calça moletom ', '75', 'M', 100, 'fotos/PicsArt_02-02-07.45.31.jpg'),
(19, 'Conjunto camisa preta e shorts bege', '88', 'G', 100, 'fotos/PicsArt_02-02-07.45.47 (1).jpg'),
(20, 'Camisa estampa games ', '60', 'M', 100, 'fotos/PicsArt_02-02-07.46.26.jpg'),
(21, 'Shorts moletom ', '70', 'G', 100, 'fotos/PicsArt_02-02-07.46.54.jpg'),
(22, 'Conjunto camisa preta e shorts listrado ', '100', 'G', 100, 'fotos/PicsArt_02-02-07.47.06.jpg'),
(23, 'Tênis Nike dunk low cinza ', '800', 'Selecione', 100, 'fotos/CV1659-001.png'),
(24, 'Tênis Nike dunk low ', '1200', 'M', 100, 'fotos/f7a3b9_b94607832d764d569a987aa4d2663730_mv2.jpg'),
(25, 'Tênis preto e branco ', '500', 'Selecione', 100, 'fotos/PicsArt_02-02-07.36.45.jpg'),
(26, 'Tênis cano alto preto', '400', 'Selecione', 100, 'fotos/PicsArt_02-02-07.37.10 (1).jpg'),
(27, 'Salto branco', '900', 'Selecione', 100, 'fotos/PicsArt_02-02-07.38.42.jpg'),
(28, 'Salto preto', '800', 'M', 100, 'fotos/PicsArt_02-02-07.39.05.jpg'),
(30, 'Sandalia infantil feminina', '140', 'Selecione', 100, 'fotos/PicsArt_02-02-07.39.43.jpg'),
(31, 'sandalia infantil feminina azul', '160', 'Selecione', 100, 'fotos/PicsArt_02-02-07.41.17 (1).jpg'),
(32, 'Salto rosa ', '900', 'Selecione', 100, 'fotos/PicsArt_02-02-07.54.40.jpg'),
(33, 'Sapato infantil ', '190', 'Selecione', 100, 'fotos/PicsArt_02-02-07.55.15 (1).jpg'),
(34, 'Sapato preto', '170', 'Selecione', 100, 'fotos/PicsArt_02-02-07.55.35.jpg'),
(36, 'Botina infantil', '100', 'Selecione', 100, 'fotos/PicsArt_02-02-07.55.46.jpg'),
(37, 'Sandalia infantil amarela ', '180', 'Selecione', 100, 'fotos/PicsArt_02-02-07.56.25.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`pkid`);

--
-- Indexes for table `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD PRIMARY KEY (`pkid`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`pkid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `pkid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `funcionarios`
--
ALTER TABLE `funcionarios`
  MODIFY `pkid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `pkid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
