<?php

$tela=1;

include('../conexao.php');

session_start();
 

 if(isset($_POST['btnEnviar'])) {
     if($_FILES['txtFoto']['error'] <> 4) {
    $arquivo=$_FILES['txtFoto']['tmp_name'];
    $caminho="fotos/".$_FILES['txtFoto']['name'];
    move_uploaded_file($arquivo, $caminho);
     }
     else {
         $caminho = $_POST["foto_atual"];
     }
    $nome=$_POST['txtNome'];
    $preco=$_POST['txtPreco'];
    $tamanho=($_POST['txtTamanho']);
    $estoque=$_POST['txtEstoque'];
    $categoria=$_POST['txtCategoria'];
    $numero=$_POST['txtNumero'];

    //ALTERA PRODUTO NO BANCO DE DADOS

     $query=mysqli_query($conecta, "UPDATE produtos SET nome='$nome', preco='$preco', tamanho='$tamanho', estoque='$estoque', foto='$caminho', categoria='$categoria', numero='$numero' WHERE pkid=".$_GET['ref']);

    if($query){
        $color=base64_encode('success');
        $status=base64_encode('Que beleza!');
        $msg=base64_encode('Cadastro realizado com sucesso!');
    } else {
        $color=base64_encode('danger');
        $status=base64_encode('Que nhaca!');
        $msg=base64_encode('Cadastro deu ruim!');
    }

    header('Location: .?msg='.$msg.'&status='.$status.'&color='.$color);
    exit;

}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Projeto :: Cadastro de Produtos</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
        integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

    <link rel="stylesheet" href="estilo.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
</head>

<body>

    <nav class="navbar navbar-light bg-light p-3">

        <?php include('../header.php') ?>

    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav id="sidebar" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">

                <?php include('../menu.php') ?>

            </nav>
            <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4 py-4">
                <nav aria-label="breadcrumb" style="line-height: 60px;">
                    <ol class="breadcrumb" style="padding-left: 25px">
                        <li class="breadcrumb-item"><a href="./home.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Cadastro de Produtos</li>
                    </ol>
                </nav>
                <h1 class="h2">Produtos</h1>
                <p>Cadastro de Produtos</p>
                <div class="row">
                    <div class="col-12 col-xl-8 mb-4 mb-lg-0">


                        <?php 
                            if(isset($_GET['msg'])) {
                        ?>

                        <div class="alert alert-<?php echo base64_decode($_GET['color']) ?> alert-dismissible fade show"
                            role="alert">
                            <strong><?php echo base64_decode($_GET['status']) ?></strong>
                            <?php echo base64_decode($_GET['msg']) ?>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>

                        <?php } ?>

                        <?php 
                        $query=mysqli_query($conecta, "SELECT * FROM produtos WHERE pkid=".$_GET['ref']);
                        $result=mysqli_fetch_assoc($query)?>

                        <form method="post" enctype="multipart/form-data">

                            <div class="mb-3">
                                <label class="form-label">Categoria</label>
                                <select class="form-select form-select-md" name="txtCategoria" autofocus
                                    aria-label=".form-select-sm example"
                                    onchange="valida(this)"
                                    onfocus="valida(this)"
                                    >
                                    <option <?php echo $result ["categoria"] == 'Masculino' ? 'selected' : ''?> value="Masculino">Masculino</option>
                                    <option <?php echo $result ["categoria"] == 'Feminino' ? 'selected' : ''?> value="Feminino">Feminino</option>
                                    <option <?php echo $result ["categoria"] == 'Infantil' ? 'selected' : ''?> value="Infantil">Infantil</option>
                                    <option <?php echo $result ["categoria"] == 'Acessorios' ? 'selected' : ''?> value="Acessorios">Acessórios</option>
                                    <option <?php echo $result ["categoria"] == 'Calcados' ? 'selected' : ''?> value="Calcados">Calçados</option>
                                </select>
                            </div>
                            <script>
                            function valida(valor) {
                                if (valor.value == 'Calcados') {
                                    document.getElementById("numero").disabled = false;
                                    document.getElementById("tamanho").disabled = true;
                                } else if (valor.value == 'Acessorios') {
                                    document.getElementById("numero").disabled = true;
                                    document.getElementById("tamanho").disabled = true;
                                } else {
                                    document.getElementById("numero").disabled = true;
                                    document.getElementById("tamanho").disabled = false;
                                }
                            }
                            </script>


                            <div class="mb-3 mt-2">
                                <label class="form-label">Nome</label>
                                <input type="text" required name="txtNome" class="form-control"
                                    placeholder="Nome do Produto" value="<?php echo $result ["nome"]?>">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Preço</label>
                                <input type="text" required name="txtPreco" class="form-control"
                                    placeholder="Ex: R$ 100,00" value="<?php echo $result ["preco"]?>">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Tamanho</label>
                                <select class="form-select form-select-md" name="txtTamanho"
                                    aria-label=".form-select-sm example" id="tamanho" disabled>
                                    <option selected value="<?php echo $result ["tamanho"]?>">
                                        <?php echo $result ["tamanho"]?></option>
                                    <option value="P">P</option>
                                    <option value="M">M</option>
                                    <option value="G">G</option>
                                    <option value="GG">GG</option>
                                </select>
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Número</label>
                                <input type="number" name="txtNumero" class="form-control" placeholder="Ex: 30"
                                    id="numero" disabled value="<?php echo $result ["numero"]?>">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Estoque</label>
                                <input type="number" required name="txtEstoque" class="form-control"
                                    placeholder="Ex: 30" value="<?php echo $result ["estoque"]?>">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Foto</label>
                                <input type="file" name="txtFoto" class="form-control" placeholder="Ex: ">
                            </div>
                            <div class="mb-3">
                                <label class="form-label"> </label>
                                <img src="<?php echo $result["foto"]?>" width="100">
                            </div>

                            <div class="mt-4">
                                <input type="hidden" name="foto_atual" value="<?php echo $result["foto"]?>">
                                <button type="submit" name="btnEnviar" class="btn btn-outline-primary">Salvar</button>
                                <a href="."  name="btnCancelar" class="btn btn-outline-danger">Cancelar</a>
                            </div>

                        </form>

                    </div>
                </div>
        </div>

        <?php include('../footer.html') ?>

        </main>
    </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
        integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>

</body>

</html>