  <!-- Footer Top Section Start -->
  <div class="footer-top-section section bg-theme-two-light section-padding">
        <div class="container">
            <div class="row mbn-40">

                <div class="footer-widget col-lg-3 col-md-6 col-12 mb-40">
                    <h4 class="title">ENTRE EM CONTATO</h4>
                    <li><a href="endereco.php"> ONDE ESTAMOS</a></li>
                    <p>                        
                 </p>
                    <p><a href="tel:01234567890">(12) 2125-6099</a><a href="tel:01234567891">(12) 2125-6099 </a></p>
                    <p><a href="mailto:info@example.com">fashionstore.com</a><a href="endereco.php">www.fashionstore.com</a></p>
                </div>

                <div class="footer-widget col-lg-3 col-md-6 col-12 mb-40">
                    <h4 class="title">PRODUTOS</h4>
                    <ul>
                        <li><a href="produtos.php?ref=<?php echo base64_encode('MASCULINO') ?>">Masculino</a></li>
                        <li><a href="produtos.php?ref=<?php echo base64_encode('FEMININO') ?>">Feminino</a></li>
                        <li><a href="produtos.php?ref=<?php echo base64_encode('INFANTIL') ?>">Infantil</a></li>
                        <li><a href="produtos.php?ref=<?php echo base64_encode('CALÇADO') ?>">Calçados</a></li>
                        <li><a href="produtos.php?ref=<?php echo base64_encode('ACESSÓRIOS') ?>">Acessórios</a></li>
                        
                    </ul>
                </div>
        

                <div class="footer-widget col-lg-3 col-md-6 col-12 mb-40">
                    <h4 class="title">INFORMAÇÃO</h4>
                    <ul>
                        <li><a href="sobre.php">Sobre nós</a></li>
                        <li><a href="termos.php">Termos e condições</a></li>
                        <li><a href="duvidas.php">dúvidas frequentes</a></li>
                        <li><a href="troca.php">troca e devoluçoes</a></li>
                        <li><a href="seguranca.php">segurança</a></li>
                        
                    </ul>
                </div>

                <div class="footer-widget col-lg-3 col-md-6 col-12 mb-40">
                    <h4 class="title">NOVIDADES</h4>
                    <p>se inscreva e receba todas as nossas novidades</p>

                    <form id="mc-form" class="mc-form footer-subscribe-form" novalidate="true">
                        <input id="mc-email" autocomplete="off" placeholder="digite seu email..." name="EMAIL"
                            type="email">
                        <button id="mc-submit"><i class="fa fa-paper-plane-o"></i></button>
                    </form>
                    <!-- mailchimp-alerts Start -->
                    <div class="mailchimp-alerts">
                        <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                        <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                        <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                    </div><!-- mailchimp-alerts end -->

                    <h5>NOSSAS REDES SÓCIAS </h5>
                    <p class="footer-social"><a href="#">Facebook</a> - <a href="#">Twitter</a> - <a
                            href="#">Google+</a></p>

                </div>

            </div>
        </div>
    </div><!-- Footer Top Section End -->

    <!-- Footer Bottom Section Start -->
    <div class="footer-bottom-section section bg-theme-two pt-15 pb-15">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <p class="footer-copyright">Copyright &copy; All rights reserved</p>
                </div>
            </div>
        </div>
    </div><!-- Footer Bottom Section End -->