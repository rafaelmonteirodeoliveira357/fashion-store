<?php

include('conecta.php');
session_start();

if(isset($_GET['ref'])){
    mysqli_query($conecta, "DELETE FROM tmpvendas WHERE pkid=".base64_decode($_GET['ref']));
    header("Location: carrinho.php");
    exit;
}

if(isset($_GET['Encerrar'])){
    if(!isset($_SESSION['nome'])){
        header("Location: login-register.php");
        exit;   
    } else {
        $busca=mysqli_query($conecta, "SELECT count(A.pkid) as totalItens, sum((B.preco) * A.quantidade) AS totalValor FROM tmpvendas A, produtos B WHERE B.pkid = A.fkidProduto AND A.datahora='" . $_SESSION['datahora'] . "'");
        $result=mysqli_fetch_assoc($busca);
        
        mysqli_query($conecta, "INSERT INTO vendas (idCliente, data, hora, total, status) VALUES ('".$_SESSION['id']."',
        '".date('Y-m-d')."',
        '".date('H:m:s')."',
        '".$result['totalValor']."',
        'Pedido Recebido')");

        $busca=mysqli_query($conecta, "SELECT *  FROM vendas ORDER BY pkid DESC LIMIT 1");
        $resultVendas=mysqli_fetch_assoc($busca);
        $codigoVenda=$resultVendas['pkid'];

        $queryProdutos=mysqli_query($conecta, "SELECT * FROM tmpvendas WHERE datahora='".$_SESSION['datahora']."'");
        while($resultProdutos=mysqli_fetch_assoc($queryProdutos)){
            mysqli_query($conecta, "INSERT INTO vendasitens (idVenda, idProduto, quantidade) VALUES (
                '".$codigoVenda."',
                '".$resultProdutos['fkidProduto']."',
                '".$resultProdutos['quantidade']."')");
        }

        mysqli_query($conecta, "DELETE FROM tmpvendas WHERE datahora='".$_SESSION['datahora']."'");

    }
}

?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>fashion store</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="assets/css/icon-font.min.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Helper CSS -->
    <link rel="stylesheet" href="assets/css/helper.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>
</head>

<body>

    <div class="main-wrapper">

        <!-- Header Section Start -->
        <div class="header-section section">

            <?php include ('menu.php') ?>

        </div><!-- Header Section End -->

    </div><!-- Banner Section End -->

    <div class="page-banner-section section" style="background-image: url(img-banner/banner-carrinho.png)">
        <div class="container">
            <div class="row">
                <div class="page-banner-content col">


                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="./">CARRINHO </a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <!-- Page Section Start -->
    <div class="page-section section section-padding">
        <div class="container">

            <form action="#">
                <div class="row mbn-40">
                    <div class="col-12 mb-40">
                        <div class="cart-table table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="pro-thumbnail">Imagem</th>
                                        <th class="pro-title">Produto</th>
                                        <th class="pro-price">Preço</th>
                                        <th class="pro-quantity">Quantidade</th>
                                        <th class="pro-subtotal">Total</th>
                                        <th class="pro-remove">Remover</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(isset($_SESSION['datahora'])) { 
                                        $busca=mysqli_query($conecta, "SELECT A.*, B.preco, B.nome, B.foto FROM tmpvendas A, produtos B WHERE B.pkid = A.fkidProduto AND A.datahora='" . $_SESSION['datahora'] . "'");
                                        while ($result=mysqli_fetch_assoc($busca)){
                                ?>
                                    <tr>
                                        <td class="pro-thumbnail"><a href="#"><img
                                                    src="painel/produtos/<?php echo $result['foto'] ?>"
                                                    alt="" /></a></td>
                                        <td class="pro-title"><a href="#"><?php echo utf8_encode($result['nome']) ?></a>
                                        </td>
                                        <td class="pro-price"><span
                                                class="amount">$<?php echo $result['preco'] ?></span></td>
                                        <td class="pro-quantity">
                                            <div class="pro-qty"><input type="text"
                                                    value="<?php echo $result['quantidade'] ?>"></div>
                                        </td>
                                        <td class="pro-subtotal">
                                            $<?php echo number_format($result['preco'] * $result['quantidade'], 2) ?>
                                        </td>
                                        <td class="pro-remove"><a
                                                href="carrinho.php?ref=<?php echo base64_encode($result['pkid']) ?>">×</a>
                                        </td>

                                    </tr>
                                    <?php } } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7 col-12 mb-40">
                        <div class="cart-buttons mb-30">
                            <input type="submit" value="Atualizar carriho" />
                            <a href="index.php">Continue Comprando</a>
                        </div>

                    </div>
                    <div class="col-lg-4 col-md-5 col-12 mb-40">
                        <div class="cart-total fix">
                            <h3>valor total do carrinho</h3>
                            <?php
                            if(isset($_SESSION['datahora'])) { 
                            $busca=mysqli_query($conecta, "SELECT count(A.pkid) as totalItens, sum((B.preco) * A.quantidade) AS totalValor FROM tmpvendas A, produtos B WHERE B.pkid = A.fkidProduto AND A.datahora='" . $_SESSION['datahora'] . "'");
                            $result=mysqli_fetch_assoc($busca);
                        }
                        ?>
                            <table>
                                <tbody>
                                    <tr class="cart-subtotal">
                                        <th>Subtotal</th>
                                        <td><span
                                                class="amount">$<?php echo number_format($result['totalValor'], 2) ?></span>
                                        </td>
                                    </tr>
                                    <tr class="order-total">
                                        <th>Total</th>
                                        <td>
                                            <strong><span
                                                    class="amount">$<?php echo number_format($result['totalValor'], 2) ?></span></strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="proceed-to-checkout section mt-30">
                                <a href="carrinho.php?Encerrar=<?php echo base64_encode('sim')?> ">Encerrar compra</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div><!-- Page Section End -->

    <?php include ('footer.php')?>

    </div>

    <!-- JS
============================================ -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.4.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Plugins JS -->
    <script src="assets/js/plugins.js"></script>
    <!-- Ajax Mail -->
    <script src="assets/js/ajax-mail.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

</body>

</html>