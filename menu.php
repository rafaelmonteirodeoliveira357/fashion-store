<?php
    error_reporting(0);
    session_start();
?>

<!-- Header Top Start -->
<div class="header-top header-top-one bg-theme-two">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">

            <div class="col mt-10 mb-10 d-none d-md-flex">
                <!-- Header Top Left Start -->
                <div class="header-top-left">
                    <p>Welcome to fashion store</p>
                    <p>Hotline: <a href="tel:0123456789">0123 456 789</a></p>
                </div><!-- Header Top Left End -->
            </div>



            <div class="col mt-10 mb-10">
                <!-- Header Shop Links Start -->
                <div class="header-top-right">
                    <?php
                        if(isset($_SESSION['nome'])){
                    ?>
                    <p><a href="minha-conta.php">Minha conta</a></p>
                    <?php } ?>
                    <?php
                        if(!isset($_SESSION['nome'])){
                    ?>
                    <p><a href="login-register.php">Cadastre-se</a><a href="login-register.php">Login</a></p>
                    <?php } else { ?>
                    <p>Bem vindo(a) <a href="minha-conta.php"><?php echo $_SESSION['nome'] ?></a></p>
                    <?php } ?>
                </div><!-- Header Shop Links End -->
            </div>

        </div>
    </div>
</div>
<!-- Header Bottom Start -->
<div class="header-bottom header-bottom-one header-sticky">
    <div class="container-fluid">
        <div class="row menu-center align-items-center justify-content-between">

            <div class="col mt-15 mb-15">
                <!-- Logo Start -->
                <div class="header-logo">
                    <a href="index.php">
                        <!-- <img src="assets/images/logo.png" alt="Jadusona"> -->
                        <h1 style="font-weight:bold">fashion store</h1>
                    </a>
                </div><!-- Logo End -->
            </div>

            <div class="col order-2 order-lg-3">
                <!-- Header Advance Search Start -->
                <div class="header-shop-links">

                    <div class="header-search">
                        <button class="search-toggle"><img src="assets/images/icons/search.png" alt="Search Toggle"><img
                                class="toggle-close" src="assets/images/icons/close.png" alt="Search Toggle"></button>
                        <div class="header-search-wrap">
                            <form action="#">
                                <input type="text" placeholder="Type and hit enter">
                                <button><img src="assets/images/icons/search.png" alt="Search"></button>
                            </form>
                        </div>
                    </div>
                    <?php
                        if(isset($_SESSION['datahora'])) { 
                            $busca=mysqli_query($conecta, "SELECT count(A.pkid) as totalItens, sum((B.preco) * A.quantidade) AS totalValor FROM tmpvendas A, produtos B WHERE B.pkid = A.fkidProduto AND A.datahora='" . $_SESSION['datahora'] . "'");
                            $result=mysqli_fetch_assoc($busca);
                        }
                    ?>
                    <div class="header-mini-cart">
                        <a href="carrinho.php"><img src="assets/images/icons/cart.png" alt="Cart">
                            <span><?php echo str_pad($result['totalItens'], 2, 0, STR_PAD_LEFT) ?>
                                ($<?php echo number_format($result['totalValor'], 2) ?>)</span></a>
                    </div>

                </div><!-- Header Advance Search End -->
            </div>

            <div class="col order-3 order-lg-2">
                <div class="main-menu">
                    <nav>
                        <ul>
                            <li class="active"><a href="index.php">CASA</a></li>
                            <li><a href="#">FAZER COMPRAS</a>
                                <ul class="sub-menu">
                                    <li><a
                                            href="produtos.php?ref=<?php echo base64_encode('masculino') ?>">Masculino</a>
                                    </li>
                                    <li><a href="produtos.php?ref=<?php echo base64_encode('feminino') ?>">Feminino</a>
                                    </li>
                                    <li><a href="produtos.php?ref=<?php echo base64_encode('infantil') ?>">Infantil</a>
                                    </li>
                                    <li><a href="produtos.php?ref=<?php echo base64_encode('calcados') ?>">Calçados</a>
                                    </li>
                                    <li><a
                                            href="produtos.php?ref=<?php echo base64_encode('acessorios') ?>">Acessórios</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#">FINALIZAR COMPRA</a>
                                <ul class="sub-menu">
                                    <li><a href="carrinho.php">CARRINHO</a></li>
                                </ul>
                            </li>
                            <!-- <li><a href="blog.html">BLOG</a>
                                        <ul class="sub-menu">
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="single-blog.html">Single Blog</a></li>
                                        </ul>
                                    </li> -->
                            <li><a href="contato.php">CONTATO</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <!-- Mobile Menu -->
            <div class="mobile-menu order-12 d-block d-lg-none col"></div>

        </div>
    </div>
</div><!-- Header BOttom End -->